/*****************************************************************************
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 *****************************************************************************/
import * as express from "express";   // import EXPRESS
import {Request, Response, NextFunction} from "express";   // import from EXPRESS
import session    = require ('express-session');

/****************************************************************************
 ***    Datenbank anbindung                                                    *
 ***************************************************************************/

import db = require ("mysql");
import {Connection, MysqlError, FieldInfo} from "mysql";

let connection: Connection = db.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_babyprodukte'
});

connection.connect(function (err: MysqlError | null) {
    if (!err) {
        console.log("Datenbank ist verbunden.");
    } else {
        console.log("Datenbank nicht verbunden.");
    }
});


class User {
    nutzer_id: number;
    vorname: string;
    nachname: string;
    benutzername: string;
    email: string;
    password: string;


    constructor(nutzer_id: number, vorname: string, nachname: string, benutzername: string, email: string, password: string) {
        this.nutzer_id = nutzer_id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.benutzername = benutzername;
        this.email = email;
        this.password = password;
    }
}



/*****************************************************************************
 ***  Create server with handler function and start it                       *
 *****************************************************************************/
let router = express();
router.listen(8080, "localhost", function () {
    console.log(`
    -------------------------------------------------------------
	          http://localhost:8080/babyworld.html
    -------------------------------------------------------------
  `);
});

class Rights {
    admin: boolean; // user is admin
    user: boolean; // user is user
    constructor(admin: boolean, user: boolean) {
        this.admin = admin;
        this.user = user;

    }
}


router.use(session({
    // save session even if not modified
    resave: true,
    // save session even if not used
    saveUninitialized: true,
    // forces cookie set on every response needed to set expiration (maxAge)
    rolling: true,
    // name of the cookie set is set by the server
    name: "mySessionCookie",
    // encrypt session-id in cookie using "secret" as modifier
    secret: "geheim",
    // set some cookie-attributes. Here expiration-date (offset in ms)
    cookie: {maxAge: 3 * 60 * 1000},
}));

/*****************************************************************************
 ***  Static routers                                                         *
 *****************************************************************************/
let baseDir: string = __dirname + '/../..';
router.use("/", express.static(baseDir + "/client/views"));
router.use("/images", express.static(baseDir + "/client/images"));
router.use("/css", express.static(baseDir + "/client/css"));
router.use("/src", express.static(baseDir + "/client/src"));
router.use("/jquery", express.static(baseDir + "/node_modules/jquery/dist"));
router.use("/popper.js", express.static(baseDir + "/node_modules/popper.js/dist"));
router.use("/bootstrap", express.static(baseDir + "/node_modules/bootstrap/dist"));
router.use("/font-awesome", express.static(baseDir + "/node_modules/font-awesome"));
router.use(express.json());

/* Zur Behebung des Chrome Fehlers */

router.use((req: Request, res, next) => {
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    console.log(req.method + " " + req.url);
    next();
})

/**
 * login with: post /login
 * @api        {post} /login
 * @apiVersion 1.0.0
 * @apiName    Login
 * @apiDescription
 * Checkt ob Benutzer mit dem Passwort vorhanden ist
 * @apiExample {url} Usage Example
 * http://localhost:8080/login
 * @apiParamExample {json} Parameters Example
 * benutzername=MaxMustermann&password=Musterpasswort
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "Mustermann ist eingeloggt.",}
 */


/* Login (Überprüfung ob Admin und entsprechende Rechtevergabe) */

router.post("/login", function (req: Request, res: Response) {
    let benutzername: string = (req.body.benutzername);
    let password: string = (req.body.password);
    let message: string = "";

    let getData: [string, string] = [benutzername, password];
    let query: string = 'SELECT * FROM tbl_nutzer WHERE benutzername = ? AND password = ?;';

    if ((benutzername != "") && (password != "")) {
        connection.query(query, getData, function (err: MysqlError | null, rows: any) {
            if (!err) {
                if (rows.length === 1) {
                    if (rows[0].benutzergruppe_id == 3) {
                        message = rows[0].benutzername + ", alias der Boss, ist eingeloggt.";
                        req.session.username = benutzername;
                        req.session.rights = new Rights(true, true);
                        res.status(200);
                        res.json({"message": message});
                    } else {
                        message = rows[0].benutzername + " ist eingeloggt.";
                        req.session.username = benutzername;
                        req.session.rights = new Rights(false, true);
                        res.status(200);
                        res.json({"message": message});
                    }
                } else {
                    message = "Benutzername und Passwort passen nicht zusammen.";
                    res.status(404);
                    res.json({"message": message});
                }
            } else {
                message = "Datenbank Fehler: " + err.code;
                res.status(500);
                res.json({"message": message});
            }
        });
    } else {
        message = "Bitte alle Felder ausfüllen.";
        res.status(400);
        res.json({"message": message});
    }
});


/******************************************************************************************
 *** Benutzer registrieren                                                              ***
 ******************************************************************************************/

/**
 * registrieren with: post /registrieren
 * @api        {post} /registrieren
 * @apiVersion 1.0.0
 * @apiName    Registrieren
 * @apiDescription
 * legt einen neuen Benutzer an
 * @apiExample {url} Usage Example
 * http://localhost:8080/registrieren
 * @apiSuccessExample {json} 201 (OK) Example
 * HTTP/1.1 201 OK
 * { "message"  : "Benutzer erstellt.",}
 */


 /* Registierung der Nutzer */

router.post("/registrieren", function (req: Request, res: Response) {
    let vorname: string = (req.body.vorname);
    let nachname: string = (req.body.nachname);
    let benutzername: string = (req.body.benutzername);
    let email: string = (req.body.email);
    let password1: string = (req.body.password1);
    let password2: string = (req.body.password2);

    let message: string = "";


    let insertData: [string, string, string, string, string] = [vorname, nachname, benutzername, email, password1];
    let query: string = 'INSERT INTO `tbl_nutzer` (`nutzer_id`, `vorname`, `nachname`, `benutzername`, `email`, `password`, `geburtsdatum`, `anschrift_id`, `benutzergruppe_id`) VALUES (NULL, ?, ?, ?, ?, ?, NULL, NULL, 1);';

    if ((vorname !== "") && (nachname !== "") && (benutzername !== "") && (email !== "") &&
        (password1 !== "") && (password2 !== "")) {
        if (password1 == password2) {
            connection.query(query, insertData, function (err: MysqlError | null, rows: any) {
                if (!err) {
                    message = "Benutzer erstellt.";
                    res.status(201);
                    res.json({"message": message});
                } else {
                    message = "Benutzername bereits vorhanden";
                    res.status(400);
                    res.json({"message": message});
                }
            });
        } else {
            message = "Passwort nicht identisch.";
            res.status(400);
            res.json({"message": message});
        }
    } else {
        message = "Bitte alle Felder korrekt ausfüllen.";
        res.status(400);
        res.json({"message": message});
    }
});

/* Ausloggen */

router.post("/logout", function (req: Request, res: Response) {
    let message: string = "Ausgeloggt.";

    req.session.username = null;
    req.session.rights = null;

    res.status(200);
    res.json({"message": message});
});


router.use(function (req: Request, res: Response, next: NextFunction) {
    let message: string = ""; // To be set
    if (req.session.username) { //--- Check if session still exists -----------------
        next();   // call subsequent routers (only if logged in)
    } else {
        message = "Bitte einloggen.";
        res.status(401);
        res.json({"message": message})
    }
});


/**
 * Benutzername löschen
 * @api        {delete}
 * @apiVersion 1.0.0
 * @apiName    delete
 * @apiDescription
 * diese route löscht bestimmte Daten des Nutzers aus der Userliste
 * @apiExample {url} Usage Example
 * http://localhost:8080/delete/:benutzername
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "Benutzername gelöscht.",}
 * { "message"  : "Benutzername nicht gefunden.",}
 * { "message"  : "Datenbank Fehler (Error 505).",}
 */

/* Admin kann alle User löschen, Nutzer kann nur sich selbst löschen  */

router.delete("/delete/:benutzername", function (req: Request, res: Response) {

    let message: string = "";

    if (req.session.rights.admin) {
        let benutzername: string = (req.params.benutzername);
        let deleteData: [string] = [benutzername];
        let query: string = 'DELETE FROM tbl_nutzer WHERE benutzername = ?;';
        connection.query(query, deleteData, function (err: MysqlError | null, rows: any) {
            if (!err) {
                if (rows.affectedRows > 0) {
                    message = benutzername + " gelöscht";
                    res.status(200);
                    res.json({"message": message});
                } else {
                    message = benutzername + " nicht gefunden";
                    res.status(404);
                    res.json({"message": message});
                }
            } else {
                message = "Datenbank Fehler: " + err.code;
                res.status(500);
                res.json({"message": message});
            }
        });

    } else {
        let benutzername : string = req.session.username;
        let deleteData: [string] = [benutzername];
        let query: string = 'DELETE FROM tbl_nutzer WHERE benutzername = ?;';
        connection.query(query, deleteData, function (err: MysqlError | null, rows: any) {
            if (!err) {
                if (rows.affectedRows > 0) {
                    message = benutzername + " gelöscht";
                    req.session.username = null;
                    req.session.rights = null;
                    res.status(200);
                    res.json({"message": message});
                } else {
                    message = benutzername + " nicht gefunden";
                    res.status(404);
                    res.json({"message": message});
                }
            } else {
                message = "Datenbank Fehler: " + err.code;
                res.status(500);
                res.json({"message": message});
            }
        });
    }
});


/**
 * Benutzer aktualiseren
 * @api        {put}
 * @apiVersion 1.0.0
 * @apiName    put
 * @apiDescription
 * diese route liest bestimmte Informationen zum Nutzer aus der datenbank aus, um die dazugehörigen
 * Informationen zum nutzer zu aktualisieren
 * @apiExample {url} Usage Example
 * http://localhost:8080/edit/:benutzername
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "Benutzername geändert",}
 * { "message"  : "Benutzername nicht gefunden.",}
 * { "message"  : "Datenbank Fehler (Error 505).",}
 * { "message"  : "Passwort nicht identisch (Error 400).",}
 * { "message"  : "Bitte alle Felder ausfüllen (Error 400).",}
 */

/*  Admin kann alle User bearbeiten, User kann nur sich selbst bearbeiten (egal welchen Benutzernamen er im Feld angibt)  */

router.put("/edit/:benutzername", function (req: Request, res: Response) {
    let vorname: string = (req.body.vorname);
    let nachname: string = (req.body.nachname);
    let email: string = (req.body.email);
    let password1: string = (req.body.password1);
    let password2: string = (req.body.password2);

    let message: string = "";

    if (req.session.rights.admin) {
        let benutzername: string = (req.params.benutzername);
        let updateData: [string, string, string, string, string] = [vorname, nachname, email, password1, benutzername];
        let query: string = 'UPDATE tbl_nutzer SET vorname = ?, nachname = ?, email = ?, password = ? WHERE benutzername = ?;';

        if ((benutzername !== "") && (vorname !== "") && (nachname !== "") && (email !== "") && (password1 !== "") && (password2 !== "")) {
            if (password1 == password2) {
                connection.query(query, updateData, function (err: MysqlError | null, rows: any) {
                    if (!err) {
                        if (rows.affectedRows == 1) {
                            message = benutzername + " geändert.";
                            res.status(201);
                            res.json({"message": message});
                        } else {
                            message = benutzername + " nicht gefunden.";
                            res.status(404);
                            res.json({"message": message});
                        }
                    } else {
                        message = "Datenbank Fehler: " + err.code;
                        res.status(500);
                        res.json({"message": message});
                    }
                });
            } else {
                message = "Passwort nicht identisch.";
                res.status(400);
                res.json({"message": message});
            }
        } else {
            message = "Bitte alle Felder ausfüllen.";
            res.status(400);
            res.json({"message": message});
        }

    } else {
        let benutzername = req.session.username;
        let updateData: [string, string, string, string, string] = [vorname, nachname, email, password1, benutzername];
        let query: string = 'UPDATE tbl_nutzer SET vorname = ?, nachname = ?, email = ?, password = ? WHERE benutzername = ?;';

        if ((benutzername !== "") && (vorname !== "") && (nachname !== "") && (email !== "") && (password1 !== "") && (password2 !== "")) {
            if (password1 == password2) {
                connection.query(query, updateData, function (err: MysqlError | null, rows: any) {
                    if (!err) {
                        if (rows.affectedRows == 1) {
                            message = benutzername + " geändert.";
                            res.status(201);
                            res.json({"message": message});
                        } else {
                            message = benutzername + " nicht gefunden.";
                            res.status(404);
                            res.json({"message": message});
                        }
                    } else {
                        message = "Datenbank Fehler: " + err.code;
                        res.status(500);
                        res.json({"message": message});
                    }
                });
            } else {
                message = "Passwort nicht identisch.";
                res.status(400);
                res.json({"message": message});
            }
        } else {
            message = "Bitte alle Felder ausfüllen.";
            res.status(400);
            res.json({"message": message});
        }

    }

});


/**
 * User anzeigen with: get
 * @api        {get}
 * @apiVersion 1.0.0
 * @apiName    GET
 * @apiDescription
 * Diese Route ermöglicht es User anzuzeigen
 * @apiExample {url} Usage Example
 * http://localhost:8080/useranzeigen
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "rows(0).benutzername",}
 * { "message"  : "Datenbank Fehler (Error 505).",}
 */

/* Admin kann sich die Benutzerliste anzeigen lassen */

router.get("/anzeigen", function (req: Request, res: Response) {
    let status: number = 500;

    let message: string = "";

    let query: string = 'SELECT * FROM tbl_nutzer;';

    let userslist: [User] = null;

    if (req.session.rights.admin){
        connection.query(query, function (err: MysqlError | null, rows: any) {

            if (!err) {
                userslist = rows;
                message = "Benutzer angezeigt.";
                status = 200;
            } else {
                message = "Datenbank Fehler: " + err.code;
                status = 500;
            }
            res.status(status).send({
                message: message,
                userlist: userslist
            });
        });
    } else {
        message = "Fehlende Berechtigung";
        res.status(403);
        res.json({"message": message});
    }
});


/**
 * produkt anlegen with: Post
 * @api        {Post}
 * @apiVersion 1.0.0
 * @apiName    Post
 * @apiDescription
 * Diese Route ermöglicht Verkäufern Produkte anlegen zu können
 * @apiExample {url} Usage Example
 * http://localhost:8080/productanlegen
 * @apiSuccessExample {json} 201 (OK) Example
 * HTTP/1.1 201 OK
 * { "message"  : " produkt angelegt",}
 * { "message"  : " Fehler" (Error 505).",}
 */

router.post("/produktanlegen", function (req: Request, res: Response) {
    let beschreibung: string = (req.body.beschreibung);
    let preis: number = (req.body.preis);
    let groesse: number = (req.body.groesse);
    let zustand_id: number = 1;
    let produktkategorie_id: number = 1;
    let message: string = "";


    let insertData: [string, number, number, number, number] = [beschreibung, preis, groesse, zustand_id, produktkategorie_id];
    let query: string = 'INSERT INTO tbl_produkt (beschreibung, preis, groesse,zustand_id,produktkategorie_id) VALUES (?, ?, ?, ?, ?);';

    if ((beschreibung !== "") && (preis > 0) && (groesse > 0)) {
        connection.query(query, insertData, function (err: MysqlError | null, rows: any) {
            if (!err) {
                message = "Produkt angelegt.";
                res.status(201);
                res.json({"message": message});
            } else {
                message = "Fehler: " + err.code;
                res.status(500);
                res.json({"message": message});
            }
        });
    } else {
        message = "Bitte Zahl in Zahl und Wort in Wort.";
        res.status(400);
        res.json({"message": message});
    }
});

/**
 * Produktandern with: Put
 * @api        {PUT}
 * @apiVersion 1.0.0
 * @apiName    PUT
 * @apiDescription
 * Diese Route ermöglicht es Produkte zu bearbeiten/ändern
 * @apiExample {url} Usage Example
 * http://localhost:8080produktandern
 * @apiSuccessExample {json} 201 (OK) Example
 * HTTP/1.1 201 OK
 * { "message"  : "Produkt geändert",}
 * { "message"  : "Fehler 21(Error 400).",}
 * * { "message"  : "Bitte Zahl in Zahl und Wort in Wort.",}
 */

router.put("/produktaendern/:id", function (req: Request, res: Response) {
    let beschreibung: string = (req.body.beschreibung);
    let preis: number = (req.body.preis);
    let groesse: number = (req.body.groesse);
    let ID: number = (req.params.id);
    let zustand_id: number = 1;
    let produktkategorie_id: number = 1;
    let bild: number = 2;
    let message: string = "";


    let insertData: [string, number, number, number, number, number, number] = [beschreibung, bild, preis, groesse, zustand_id, produktkategorie_id, ID];
    let query: string = 'UPDATE tbl_produkt SET beschreibung = ?, bild = ?, preis = ?, groesse = ?, zustand_id = ?, produktkategorie_id = ? WHERE produkt_id = ?;';


    if ((beschreibung !== "") && (preis > 0) && (groesse > 0)) {
        connection.query(query, insertData, function (err: MysqlError | null, rows: any) {
            if (!err) {
                message = "Produkt geändert.";
                res.status(201);
                res.json({"message": message});
            } else {
                message = "Fehler: " + err.code;
                res.status(500);
                res.json({"message": message});
            }
        });
    } else {
        message = "Bitte Zahl in Zahl und Wort in Wort.";
        res.status(400);
        res.json({"message": message});
    }
});
/**
 * User anzeigen with: get
 * @api        {get}
 * @apiVersion 1.0.0
 * @apiName    GET
 * @apiDescription
 * Diese Route ermöglicht es User anzuzeigen
 * @apiExample {url} Usage Example
 * http://localhost:8080/useranzeigen
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "rows(0).benutzername",}
 * { "message"  : "Datenbank Fehler (Error 505).",}
 */


/*  User bekommt seinen benutzername angezeigt   */

router.get ("/useranzeigen", function (req: Request, res: Response) {
    let benutzername = req.session.username;

    let message: string = "";

    let data: [string] = [benutzername];
    let query: string = 'SELECT * FROM tbl_nutzer WHERE benutzername = ?;';


    connection.query(query, data, function (err: MysqlError | null, rows: any) {

        if (!err) {
            message = rows[0].benutzername;
            res.status(200);
        } else {
            message = "Datenbank Fehler: " + err.code;
            res.status(500);
        }
        res.json({"message": message});
    });
});

/**
 * product loeschen with: Delete
 * @api        {Delete}
 * @apiVersion 1.0.0
 * @apiName    Delete
 * @apiDescription
 * Diese Route ermöglicht Verkäufer Produkte zu löschen
 * @apiExample {url} Usage Example
 * http://localhost:8080/delete prodcut
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "produkt gelöscht",}
 * { "message"  : "Produkt nicht vorhanden.,}
 */


router.delete("/deleteproduct/:id", function (req: Request, res: Response) {
    let id: number = Number(req.params.id);

    let message: string = "";
    let deleteData: [number] = [id];
    let query: string = 'DELETE FROM tbl_produkt WHERE  produkt_id = ?;';
    connection.query(query, deleteData, function (err: MysqlError | null, rows: any) {

        if (!err) {
            if (rows.affectedRows > 0) {
                message = id + " Produkt gelöscht";
                res.status(200);
                res.json({"message": message});
            } else {
                message = id + " produkt nicht vorhanden";
                res.status(404);
                res.json({"message": message});
            }
        }
    });
});





