-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Jul 2018 um 18:54
-- Server-Version: 10.1.32-MariaDB
-- PHP-Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_babyprodukte`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_anschrift`
--

CREATE TABLE `tbl_anschrift` (
  `anschrift_id` int(10) NOT NULL,
  `strasse` varchar(50) NOT NULL,
  `hausnummer` int(10) NOT NULL,
  `plz` int(5) NOT NULL,
  `ort` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_anschrift`
--

INSERT INTO `tbl_anschrift` (`anschrift_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES
(1, 'Westring', 15, 56478, 'Langendernbach'),
(2, 'Ackerbau', 23, 23584, 'Breuenfeld'),
(3, 'Ringstrasse', 31, 94839, 'Butzbach'),
(4, 'Kingsroad', 99, 89384, 'Kingslanding'),
(6, 'Westring', 14, 56479, 'Irmtraut');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_benutzergruppe`
--

CREATE TABLE `tbl_benutzergruppe` (
  `benutzergruppe_id` int(10) NOT NULL,
  `beschreibung` varchar(100) NOT NULL,
  `rechte_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_benutzergruppe`
--

INSERT INTO `tbl_benutzergruppe` (`benutzergruppe_id`, `beschreibung`, `rechte_id`) VALUES
(1, 'Nutzer', 1),
(2, 'VIP-Nutzer', 2),
(3, 'Admin', 3),
(4, 'Portalanbieter', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_bestellung`
--

CREATE TABLE `tbl_bestellung` (
  `bestellung_id` int(10) NOT NULL,
  `kaufdatum` date DEFAULT NULL,
  `versanddatum` date DEFAULT NULL,
  `zustellungsdatum` date DEFAULT NULL,
  `verkaeufer_id` int(11) NOT NULL,
  `kaeufer_id` int(11) DEFAULT NULL,
  `produkt_id` int(11) NOT NULL,
  `versand_id` int(11) DEFAULT NULL,
  `bezahlart` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_bewertung`
--

CREATE TABLE `tbl_bewertung` (
  `bewertung_id` int(10) NOT NULL,
  `bewertung` varchar(250) NOT NULL,
  `punkte` int(1) NOT NULL,
  `datum` date NOT NULL,
  `bewerteter` int(11) NOT NULL,
  `bewerter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_nutzer`
--

CREATE TABLE `tbl_nutzer` (
  `nutzer_id` int(10) NOT NULL,
  `vorname` varchar(50) NOT NULL,
  `nachname` varchar(50) NOT NULL,
  `benutzername` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `geburtsdatum` date DEFAULT NULL,
  `anschrift_id` int(11) DEFAULT NULL,
  `benutzergruppe_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_nutzer`
--

INSERT INTO `tbl_nutzer` (`nutzer_id`, `vorname`, `nachname`, `benutzername`, `email`, `password`, `geburtsdatum`, `anschrift_id`, `benutzergruppe_id`) VALUES
(1, 'Admin', 'Admin', 'Admin', 'Admin', 'password', NULL, 1, 3),
(3, 'Jens', 'Peter', 'jensi', 'jens.peter@gmx.de', 'blablabla', NULL, NULL, NULL),
(4, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', NULL, NULL, NULL),
(7, 'Steffen', 'Jung', 'Steffen.Jung', 'steffen.jung@gmail.com', 'password12345', NULL, NULL, NULL),
(8, 'Testuservorname', 'Testusernachname', 'Testusernutzername', 'test@testemail.com', 'testapssword', NULL, NULL, NULL),
(10, 'asdf', 'qwer', 'qwer', 'qwer', 'qwer', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_produkt`
--

CREATE TABLE `tbl_produkt` (
  `produkt_id` int(10) NOT NULL,
  `beschreibung` varchar(150) DEFAULT NULL,
  `preis` decimal(10,0) NOT NULL,
  `bild` int(11) NOT NULL,
  `marke` varchar(100) DEFAULT NULL,
  `laenge` decimal(10,0) DEFAULT NULL,
  `breite` decimal(10,0) DEFAULT NULL,
  `hoehe` decimal(10,0) DEFAULT NULL,
  `groeße` varchar(30) DEFAULT NULL,
  `gewicht` decimal(10,0) DEFAULT NULL,
  `zustand_id` int(11) NOT NULL,
  `produktkategorie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_produkt`
--

INSERT INTO `tbl_produkt` (`produkt_id`, `beschreibung`, `preis`, `bild`, `marke`, `laenge`, `breite`, `hoehe`, `groeße`, `gewicht`, `zustand_id`, `produktkategorie_id`) VALUES
(1, 'Kinder Oberteil ', '10', 7565, 'Joris', NULL, NULL, NULL, NULL, NULL, 2, 1),
(2, 'Teddybär', '15', 2345, 'Goldbären', NULL, NULL, NULL, NULL, NULL, 2, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_produktkategorie`
--

CREATE TABLE `tbl_produktkategorie` (
  `produktkategorie_id` int(10) NOT NULL,
  `kategorie` varchar(100) NOT NULL,
  `beschreibung` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_produktkategorie`
--

INSERT INTO `tbl_produktkategorie` (`produktkategorie_id`, `kategorie`, `beschreibung`) VALUES
(1, 'Kleidung Kind', 'Kleidung für Kinder ab 3 Jahren'),
(2, 'Kleidung Baby', 'Kleidung für Babys unter 3 Jahren'),
(3, 'Spielzeug', 'Kinderspielzeug');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_rechte`
--

CREATE TABLE `tbl_rechte` (
  `rechte_id` int(10) NOT NULL,
  `kategorie` varchar(30) NOT NULL,
  `1recht` varchar(30) DEFAULT NULL,
  `2recht` varchar(30) DEFAULT NULL,
  `3recht` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_rechte`
--

INSERT INTO `tbl_rechte` (`rechte_id`, `kategorie`, `1recht`, `2recht`, `3recht`) VALUES
(1, 'Nutzer', 'Verkaufen', 'Kaufen', NULL),
(2, 'VIP-Nutzer', 'Verkaufen', 'Kaufen', 'Expressversand'),
(3, 'Administrator', 'Webseite bearbeiten', 'Daten auslesen', NULL),
(4, 'Portalanbieter', 'Daten auslesen', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_retoure`
--

CREATE TABLE `tbl_retoure` (
  `retoure_id` int(10) NOT NULL,
  `bestellung_id` int(11) NOT NULL,
  `schaden` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_versand`
--

CREATE TABLE `tbl_versand` (
  `versand_id` int(10) NOT NULL,
  `beschreibung` varchar(100) NOT NULL,
  `preis` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_versand`
--

INSERT INTO `tbl_versand` (`versand_id`, `beschreibung`, `preis`) VALUES
(1, 'Standart', '2'),
(2, 'Express', '5');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_zustand`
--

CREATE TABLE `tbl_zustand` (
  `zustand_id` int(10) NOT NULL,
  `kategorie` varchar(100) NOT NULL,
  `beschreibung` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_zustand`
--

INSERT INTO `tbl_zustand` (`zustand_id`, `kategorie`, `beschreibung`) VALUES
(1, 'Neu', 'Der Artikel ist neu. '),
(2, 'Gut', 'Der Artikel befindet sich in einem guten Zustand.'),
(3, 'Schlecht', 'Der Artikel befindet sich in einem schlechten Zustand.');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_anschrift`
--
ALTER TABLE `tbl_anschrift`
  ADD PRIMARY KEY (`anschrift_id`);

--
-- Indizes für die Tabelle `tbl_benutzergruppe`
--
ALTER TABLE `tbl_benutzergruppe`
  ADD PRIMARY KEY (`benutzergruppe_id`),
  ADD KEY `rechte_id` (`rechte_id`);

--
-- Indizes für die Tabelle `tbl_bestellung`
--
ALTER TABLE `tbl_bestellung`
  ADD PRIMARY KEY (`bestellung_id`),
  ADD KEY `produkt_id` (`produkt_id`),
  ADD KEY `versand_id` (`versand_id`),
  ADD KEY `nutzer_id` (`verkaeufer_id`),
  ADD KEY `kaeufer_id` (`kaeufer_id`),
  ADD KEY `bezahlart_id` (`bezahlart`);

--
-- Indizes für die Tabelle `tbl_bewertung`
--
ALTER TABLE `tbl_bewertung`
  ADD PRIMARY KEY (`bewertung_id`),
  ADD KEY `bewerteter` (`bewerteter`),
  ADD KEY `bewerter` (`bewerter`),
  ADD KEY `bewertung_id` (`bewertung_id`);

--
-- Indizes für die Tabelle `tbl_nutzer`
--
ALTER TABLE `tbl_nutzer`
  ADD PRIMARY KEY (`nutzer_id`),
  ADD UNIQUE KEY `benutzername` (`benutzername`),
  ADD UNIQUE KEY `anschrift_id_2` (`anschrift_id`),
  ADD KEY `anschrift_id` (`anschrift_id`),
  ADD KEY `benutzergruppe_id` (`benutzergruppe_id`);

--
-- Indizes für die Tabelle `tbl_produkt`
--
ALTER TABLE `tbl_produkt`
  ADD PRIMARY KEY (`produkt_id`),
  ADD KEY `zustand_id` (`zustand_id`),
  ADD KEY `produktkategorie_id` (`produktkategorie_id`);

--
-- Indizes für die Tabelle `tbl_produktkategorie`
--
ALTER TABLE `tbl_produktkategorie`
  ADD PRIMARY KEY (`produktkategorie_id`);

--
-- Indizes für die Tabelle `tbl_rechte`
--
ALTER TABLE `tbl_rechte`
  ADD PRIMARY KEY (`rechte_id`);

--
-- Indizes für die Tabelle `tbl_retoure`
--
ALTER TABLE `tbl_retoure`
  ADD PRIMARY KEY (`retoure_id`),
  ADD KEY `schaden_id` (`schaden`),
  ADD KEY `bestellung_id` (`bestellung_id`);

--
-- Indizes für die Tabelle `tbl_versand`
--
ALTER TABLE `tbl_versand`
  ADD PRIMARY KEY (`versand_id`);

--
-- Indizes für die Tabelle `tbl_zustand`
--
ALTER TABLE `tbl_zustand`
  ADD PRIMARY KEY (`zustand_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_anschrift`
--
ALTER TABLE `tbl_anschrift`
  MODIFY `anschrift_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `tbl_benutzergruppe`
--
ALTER TABLE `tbl_benutzergruppe`
  MODIFY `benutzergruppe_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `tbl_bestellung`
--
ALTER TABLE `tbl_bestellung`
  MODIFY `bestellung_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_bewertung`
--
ALTER TABLE `tbl_bewertung`
  MODIFY `bewertung_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_nutzer`
--
ALTER TABLE `tbl_nutzer`
  MODIFY `nutzer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT für Tabelle `tbl_produkt`
--
ALTER TABLE `tbl_produkt`
  MODIFY `produkt_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `tbl_produktkategorie`
--
ALTER TABLE `tbl_produktkategorie`
  MODIFY `produktkategorie_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `tbl_rechte`
--
ALTER TABLE `tbl_rechte`
  MODIFY `rechte_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `tbl_retoure`
--
ALTER TABLE `tbl_retoure`
  MODIFY `retoure_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_versand`
--
ALTER TABLE `tbl_versand`
  MODIFY `versand_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `tbl_zustand`
--
ALTER TABLE `tbl_zustand`
  MODIFY `zustand_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_benutzergruppe`
--
ALTER TABLE `tbl_benutzergruppe`
  ADD CONSTRAINT `tbl_benutzergruppe_ibfk_1` FOREIGN KEY (`rechte_id`) REFERENCES `tbl_rechte` (`rechte_id`);

--
-- Constraints der Tabelle `tbl_bestellung`
--
ALTER TABLE `tbl_bestellung`
  ADD CONSTRAINT `tbl_bestellung_ibfk_1` FOREIGN KEY (`versand_id`) REFERENCES `tbl_versand` (`versand_id`),
  ADD CONSTRAINT `tbl_bestellung_ibfk_4` FOREIGN KEY (`produkt_id`) REFERENCES `tbl_produkt` (`produkt_id`),
  ADD CONSTRAINT `tbl_bestellung_ibfk_5` FOREIGN KEY (`verkaeufer_id`) REFERENCES `tbl_nutzer` (`nutzer_id`),
  ADD CONSTRAINT `tbl_bestellung_ibfk_6` FOREIGN KEY (`kaeufer_id`) REFERENCES `tbl_nutzer` (`nutzer_id`);

--
-- Constraints der Tabelle `tbl_bewertung`
--
ALTER TABLE `tbl_bewertung`
  ADD CONSTRAINT `tbl_bewertung_ibfk_1` FOREIGN KEY (`bewerteter`) REFERENCES `tbl_nutzer` (`nutzer_id`),
  ADD CONSTRAINT `tbl_bewertung_ibfk_2` FOREIGN KEY (`bewerter`) REFERENCES `tbl_nutzer` (`nutzer_id`);

--
-- Constraints der Tabelle `tbl_nutzer`
--
ALTER TABLE `tbl_nutzer`
  ADD CONSTRAINT `tbl_nutzer_ibfk_1` FOREIGN KEY (`anschrift_id`) REFERENCES `tbl_anschrift` (`anschrift_id`),
  ADD CONSTRAINT `tbl_nutzer_ibfk_4` FOREIGN KEY (`benutzergruppe_id`) REFERENCES `tbl_benutzergruppe` (`benutzergruppe_id`);

--
-- Constraints der Tabelle `tbl_produkt`
--
ALTER TABLE `tbl_produkt`
  ADD CONSTRAINT `tbl_produkt_ibfk_2` FOREIGN KEY (`zustand_id`) REFERENCES `tbl_zustand` (`zustand_id`),
  ADD CONSTRAINT `tbl_produkt_ibfk_3` FOREIGN KEY (`produktkategorie_id`) REFERENCES `tbl_produktkategorie` (`produktkategorie_id`);

--
-- Constraints der Tabelle `tbl_retoure`
--
ALTER TABLE `tbl_retoure`
  ADD CONSTRAINT `tbl_retoure_ibfk_3` FOREIGN KEY (`bestellung_id`) REFERENCES `tbl_bestellung` (`bestellung_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
