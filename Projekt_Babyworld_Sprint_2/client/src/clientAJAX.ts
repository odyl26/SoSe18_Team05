namespace AJAX {

    class User {
        nutzer_id: number;
        vorname: string;
        nachname: string;
        benutzername: string;
        email: string;
        password: string;

    }

    function renderuserlist(message: string, userList: User[]): void {
        let buffer: string = "";

        buffer += "<div>" + message + "</div>\n";
        if (userList.length > 0) {
            buffer += "<div>\n";
            buffer += "  <table class='table table-hover'>\n";
            buffer += "    <tr>\n";
            buffer += "      <th style='width:100px'> Benutzername  </th>\n";
            buffer += "      <th style='width:100px'> Vorname </th>\n";
            buffer += "      <th style='width:100px'> Nachname     </th>\n";
            buffer += "      <th style='width:100px'> E-Mail  </th>\n";
            buffer += "      <th style='width:100px'> Operation  </th>\n";
            buffer += "    </tr>\n";
            for (let user of userList) { // iterate through array "userData"
                if (user != null) {  // ignore array-elements that have been deleted
                    buffer += "    <tr>\n";
                    buffer += "      <td class='col-6 text-left'> " + user.benutzername + " </td>\n";
                    buffer += "      <td class='col-6 text-left'> " + user.vorname + " </td>\n";
                    buffer += "      <td class='col-6 text-left'> " + user.nachname + " </td>\n";
                    buffer += "      <td class='col-6 text-left'> " + user.email + " </td>\n";
                    buffer += "      <td class='col-6 text-left'> <i class=\"fas fa-pencil-alt\"></i>\n" +
                        "\n <i class=\"fas fa-trash-alt\"></i>\n";
                    buffer += "    </tr>\n";

                }
            }
        }
        buffer += "  </table>\n";
        buffer += "</div>";
        $('#data').html(buffer);
    }


    function render(message: string) {
        let buffer: string = "";
        buffer += alert(message);
        $('#data').html(buffer);
    }

    function renderuser(message: string){
        let buffer : string = "";
        buffer += alert("Sie sind als " + message + " eingeloggt");
        $('#data').html(buffer);
    }

    function clearInput() {
        $('#vornameInput').val("");
        $('#nachnameInput').val("");
        $('#benutzernameInput').val("");
        $('#emailInput').val("");
        $('#password1Input').val("");
        $('#password2Input').val("");
        $('#strasseInput').val("");
        $('#hausnummerInput').val("");
        $('#plzInput').val("");
        $('#ortInput').val("");
        $('#benutzername').val("");
        $('#password').val("");
    }

    $(function () {
        /* zeigt für registieren den eigentlich nicht sichtbaren Bereich an */
        $('#registrieren').on("click", () => {
            $('#BoxProfilseite').show();
        });

        /*  Registrierung  */

        $('#registrierenBtn').on("click", () => {
            let vorname: string = ($('#vornameInput').val() as string).trim();
            let nachname: string = ($('#nachnameInput').val() as string).trim();
            let benutzername: string = ($('#benutzernameInput').val() as string).trim();
            let email: string = ($('#emailInput').val() as string).trim();
            let password1: string = ($('#password1Input').val() as string).trim();
            let password2: string = ($('#password2Input').val() as string).trim();
            let data: Object = {
                "vorname": vorname,
                "nachname": nachname,
                "benutzername": benutzername,
                "email": email,
                "password1": password1,
                "password2": password2
            };

            $.ajax({                // set up ajax request
                url: 'http://localhost:8080/registrieren',
                type: 'POST',    // POST-request for CREATE
                data: JSON.stringify(data),
                contentType: 'application/json',  // using json in request
                dataType: 'json',              // expecting json in response
                success: (data) => {
                    render(data.message)
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
            clearInput();
        });

        /*  Login  */


        $('#loginBtn').on("click", () => {
            let benutzername: string = ($('#benutzername').val() as string);
            let password: string = ($('#password').val() as string);
            let data: object = {"benutzername": benutzername, "password": password};
            $.ajax({
                url: 'http://localhost:8080/login',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                success: (data) => {
                    render(data.message)
                    $('#navbarDropdown').hide();
                    $('#registrieren').hide();
                    $('#logoutBtn').show();
                    $('#BoxProfilseite').show();
                    $('#BoxNachrichten').show();
                    $('#adminBtn').show();
                    $('#BoxAdmin').show();
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
            clearInput();
        });

        /*  User löschen  */

        $('#deleteBtn').on("click", () => {
            let benutzername: string = ($('#benutzernameInput').val() as string);

            $.ajax({
                url: 'http://localhost:8080/delete/' + benutzername,
                type: 'DELETE',
                dataType: 'json',
                success: (data) => {
                    render(data.message)
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
            clearInput();
        });

        /*  Userdaten bearbeiten  */

        $('#editBtn').on("click", () => {
            let vorname: string = ($('#vornameInput').val() as string).trim();
            let nachname: string = ($('#nachnameInput').val() as string).trim();
            let benutzername: string = ($('#benutzernameInput').val() as string).trim();
            let email: string = ($('#emailInput').val() as string).trim();
            let password1: string = ($('#password1Input').val() as string).trim();
            let password2: string = ($('#password2Input').val() as string).trim();

            let data: object = {
                "vorname": vorname,
                "nachname": nachname,
                "benutzername": benutzername,
                "email": email,
                "password1": password1,
                "password2": password2
            };

            $.ajax({
                url: 'http://localhost:8080/edit/' + benutzername,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                success: (data) => {
                    render(data.message)
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
            clearInput();
        });

        /*  Anzeigen aller Benutzer (als Admin)  */

        $('#adminBtn').on("click", () => {
            $.ajax({
                url: 'http://localhost:8080/anzeigen',
                type: 'GET',
                dataType: 'json',
                success: (data) => {
                    renderuserlist(data.message, data.userlist)
                },
                error: (jqXHR) => {
                    renderuserlist(jqXHR.responseJSON.message, [])
                }
            });
        });

        /*  Logout  */

        $('#logoutBtn').on("click", () => {
            $.ajax({
                url: 'http://localhost:8080/logout',
                type: 'POST',
                dataType: 'json',
                success: (data) => {
                    render(data.message);
                    $('#navbarDropdown').show();
                    $('#registrieren').show();
                    $('#logoutBtn').hide();
                    $('#BoxProfilseite').hide();
                    $('#BoxNachrichten').hide();
                    $('#adminBtn').hide();
                    $('#BoxAdmin').show();
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
        });
        /*---------------------------------------------------------------------------------------------------------*/

        /* Zweite Persona - Verkäufer - Produktbeschreibung anlegen (CRUD-POST)--------*/


        $('#produktanlegenBtn').on("click", () => {
            console.log("hier");
            let beschreibung: string = ($('#beschreibungInput').val() as string).trim();
            let preis: number = ($('#preisInput').val() as number);
            let groesse: number = ($('#groesseInput').val() as number);
            let data: Object = {
                "beschreibung": beschreibung,
                "preis": preis,
                "groesse": groesse,

            };
            console.log("produkt anlegen");

            $.ajax({                // set up ajax request
                url: 'http://localhost:8080/produktanlegen',
                type: 'POST',    // POST-request for CREATE
                data: JSON.stringify(data),
                contentType: 'application/json',  // using json in request
                dataType: 'json',              // expecting json in response
                success: (data) => {
                    render(data.message)
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
            clearInput();
        });
        $('#produktändernBtn').on("click", () => {
            let beschreibung: string = ($('#beschreibungInput').val() as string).trim();
            let preis: number = ($('#preisInput').val() as number);
            let groesse: number = ($('#groesseInput').val() as number);
            let ID: number = ($('#IDInput').val() as number);
            let data: Object = {
                "beschreibung": beschreibung,
                "preis": preis,
                "groesse": groesse,
                "ID": ID,
            };
            console.log("produkt ändern");

            $.ajax({
                url: 'http://localhost:8080/produktaendern/' + ID,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                success: (data) => {
                    render(data.message)
                    clearInput();
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });

        });


        /* Zweite Persona, Verkäufer- Produkt anzeigen (CRUD-GET)-------------------------*/


        $('#BenutzernameanzeigenBtn').on("click", () => {
            $.ajax({
                url: 'http://localhost:8080/useranzeigen',
                type: 'GET',
                dataType: 'json',
                success: (data) => {
                    renderuser(data.message)
                },
                error: (jqXHR) => {
                    renderuser(jqXHR.responseJSON.message)
                }
            });
        });


        $('#produktloeschenBtn').on("click", () => {
            let beschreibung: string = ($('#beschreibungInput').val() as string);
            let preis: string = ($('#preisInput').val() as string);
            let groesse: string = ($('#groesseInput').val() as string);

            let id: number = Number($('#IDInput').val());

            $.ajax({
                url: 'http://localhost:8080/deleteproduct/' + id,
                type: 'DELETE',
                dataType: 'json',
                success: (data) => {
                    render(data.message)
                },
                error: (jqXHR) => {
                    render(jqXHR.responseJSON.message)
                }
            });
        });

    })
}






